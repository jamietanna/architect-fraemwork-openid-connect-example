const fetch = require('node-fetch');
const { mf2 } = require("microformats-parser");

async function discover(me) {
  if (me === undefined) {
    throw new Error('Bad profile')
  }

  const page = await fetch(me)
    .then(res => res.text())
  const parsed = mf2(page, {
    baseUrl: me
  })
  const metadatas = parsed.rels['indieauth-metadata']
  if (metadatas === undefined) {
    throw new Error(`No indieauth-metadata`)
  }

  return metadatas[0]
}

module.exports = {
  discover
}
