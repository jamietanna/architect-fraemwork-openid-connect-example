const AUTH_TIME = 1000 * 60 * 60;

function authHasExpired(auth_time, now) {
  return (now - auth_time) >= AUTH_TIME;
}

async function isLoggedIn(session) {
  if (session === undefined || !session) {
    return false
  }
  if (session.auth_me === undefined || session.auth_time === undefined) {
    return false
  }
  if (authHasExpired(session.auth_time, new Date().getTime())) {
    return false;
  }

  return true;
}

async function redirectUri() {
  return process.env.BASE_URL + 'callback'
}

async function createClient(issuer) {
  return new issuer.Client({
    client_id: process.env.BASE_URL,
    redirect_uris: [await redirectUri()],
    response_types: ['code'],
    token_endpoint_auth_method: 'none'
  });
}

module.exports = {
  isLoggedIn,
  createClient,
  redirectUri
}
