const arc = require('@architect/functions')

const auth = require('@architect/shared/auth')
const profile = require('@architect/shared/profile')
const oidc = require('openid-client')

async function handler(req) {
  var discovery
  try {
    discovery = await profile.discover(req.queryStringParameters?.me)
  } catch (e) {
    return {
      status: 400,
      html: e.message
    }
  }

  const session = req.session
  session.discovery = discovery;

  const issuer = await oidc.Issuer.discover(session.discovery);

  const client = await auth.createClient(issuer)
  const code_verifier = oidc.generators.codeVerifier();
  session.verifier = code_verifier;

  const code_challenge = oidc.generators.codeChallenge(code_verifier);

  const authorizationUrl = client.authorizationUrl({
    scope: 'profile',
    code_challenge,
    code_challenge_method: 'S256',
  });

  return {
    status: 302,
    session,
    headers: {
      location: authorizationUrl
    }
  }
}

exports.handler = arc.http.async(handler)
