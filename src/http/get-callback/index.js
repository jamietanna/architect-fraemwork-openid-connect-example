const arc = require('@architect/functions')
const auth = require('@architect/shared/auth')

const oidc = require('openid-client')

async function handler(req) {
  const session = req.session

  const issuer = await oidc.Issuer.discover(session.discovery);
  const client = await auth.createClient(issuer)
  const code_verifier = session.verifier;

  const params = req.queryStringParameters
  const redirect = await auth.redirectUri()
  const tokenSet = await client.oauthCallback(redirect, params, { code_verifier });

  session.auth_time = new Date().getTime()
  session.auth_me = tokenSet.me;

  return {
    session,
    html: `<pre>${JSON.stringify(tokenSet)}</pre>`
  }
}

exports.handler = arc.http.async(handler)
