const arc = require('@architect/functions')
const auth = require('@architect/shared/auth')

async function handler(req) {
  const session = req.session
  const isLoggedIn = await auth.isLoggedIn(session);
  if (!isLoggedIn) {
    delete session.auth_me;
    delete session.auth_time;
    return {
      session,
      html: `You're not logged in`
    }
  }

  return {
    session,
    html: `Welcome back ${session.auth_me}`
  }
}

exports.handler = arc.http.async(handler)
